import React from 'react';
import classNames from 'classnames';
import Repo from '../Repository';
import PerfectScrollbar from 'react-perfect-scrollbar'
import moment from 'moment';

export default class Messaging extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            messageItems: [],
            message: "",
            next: ""
        };

        this.messageSubscription = null;

        this.handleNextMessage = this.handleNextMessage.bind(this);
        this.handleError = this.handleError.bind(this);

        this._scrollRef = null;
    }

    sendMessage(chatId, message) {
        Repo.sendMessage(chatId, message);
    }

    handleMessageInput = e => {
        if (e.keyCode === 13 && !e.shiftKey) {
            this.sendMessage(this.props.chatId, this.state.message);
            this.setState({ message: "" })
        }
    }

    componentDidMount() {
        if (this.props.chatId) {
            Repo.getMessages(this.props.chatId, this.state.next, 20)
                .then(data => {
                    this.setState(state => ({
                        next: data.hasNext ? data.next : "",
                        messageItems: [...state.messageItems].concat(data.results)
                    }), () => {
                        this.scrollToBottom();
                    });
                });

            Repo.unsubscribe();
            this.messageSubscription = Repo.subscribeMessageSent(this.props.chatId, this.handleNextMessage, this.handleError);
        }
    }

    handleNextMessage(newMessage) {
        this.setState(state => ({
            messageItems: [...state.messageItems, newMessage]
        }), () => {
            this.scrollToBottom();
        });
    }

    handleError(error) {
        console.log(error);
    }

    componentWillUnmount() {
        Repo.unsubscribe();
    }

    scrollToBottom() {
        if (this._scrollRef) {
            this._scrollRef.scrollTop = this._scrollRef.scrollHeight;
        }
    }

    render() {
        const messageList = this.state.messageItems.map(msg =>
            (
                <div className={classNames(["message-item", { "me": msg.sender.id === this.props.loggedInUser.id }])}>
                    <div className="sender">
                        <div className="sender-name">{msg.sender.displayName}</div>
                    </div>
                    <div className="message-body">{msg.body}</div>
                    <div className="time">{moment(msg.createdAt).format("DD MMM, YYYY HH:mm:ss")}</div>
                </div>
            )
        );

        return (
            <div className="messaging">
                <div className="messages-container">
                    <PerfectScrollbar containerRef={ref => this._scrollRef = ref}>
                        <div className="messages-list" ref={ref => this._messageListRef = ref}>
                            {messageList}
                        </div>
                    </PerfectScrollbar>
                </div>
                <div className="input-container">
                    <textarea onKeyUp={this.handleMessageInput} value={this.state.message} onChange={e => this.setState({ message: e.target.value })}></textarea>
                </div>
            </div>
        );
    }
}