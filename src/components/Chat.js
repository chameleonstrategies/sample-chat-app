import React from 'react';
import Repo from '../Repository';
import LoginFormContainer from '../containers/LoginFormContainer';
import Messaging from '../components/Messaging';
import ChatListContainer from '../containers/ChatListContainer';

export default function Chat(props) {


    if (!props.loggedInUser) {
        return (
            <div className="chat-container">
                <LoginFormContainer />
            </div>
        );
    } else {
        Repo.getChats()
            .then(chats => {
                props.setChats(chats.results);
            })
            .catch(console.log);

        return (
            <div className="chat-container">
                <div className="main-box">
                    {props.selectedChat ? <Messaging loggedInUser={props.loggedInUser} chatId={props.selectedChat.id} /> : null}
                </div>
                <div className="chat-list-container">
                    <ChatListContainer />
                </div>
            </div>
        );
    }
}