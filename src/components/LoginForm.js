import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import repo from '../Repository';

export default function LoginForm(props) {
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [errorMsg, setErrorMsg] = React.useState("");

    const handleSubmit = e => {
        e.preventDefault();

        repo.login(email, password)
            .then(result => {
                props.setLogin(result.user, result.token);
            })
            .catch(err => {
                setErrorMsg(err.message);
            });
    }
    return (
        <form className="login-form" onSubmit={handleSubmit}>
            <h1>Login</h1>
            <p>
                <TextField id="email" label="email" type="email" fullWidth value={email} onChange={e => setEmail(e.target.value)} />
            </p>
            <p>
                <TextField id="password" label="password" type="password" fullWidth value={password} onChange={e => setPassword(e.target.value)} />
            </p>
            <FormControl>
                <Button type="submit" fullWidth variant="contained" color="primary">Login</Button>
            </FormControl>
            {errorMsg ? <div class="error">{errorMsg}</div> : null}
        </form>
    );
}