import React from 'react';
import classNames from 'classnames';

export default function ChatList(props) {

    const items = props.chats.map((chat, index) => (
        <li className={classNames({ selected: props.selectedChat && props.selectedChat.id === chat.id })} key={index} onClick={() => props.setSelectedChat(chat)}>{chat.title}</li>
    ));

    return (
        <ul>
            {items}
        </ul>
    );
}