import React from 'react';

import Button from '@material-ui/core/Button';

export default function LogoutButton(props) {

  const logout = () => {
    props.logout();
    localStorage.removeItem("loggedin");
  }

  const LoginButton = props.loggedInUser ? (<Button color="inherit" onClick={logout}>Logout</Button>) : null

  return (
    <div className="login-widget">
      {LoginButton}
    </div>
  );
}