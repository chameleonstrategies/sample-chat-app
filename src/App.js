import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import 'react-perfect-scrollbar/dist/css/styles.css';
import './App.scss';

import Repo from './Repository';

import NavBar from './components/NavBar';
import ChatContainer from './containers/ChatContainer';

function App() {

  return (
    <Provider store={store}>
      <div className="App">
        <NavBar />
        <ChatContainer />
      </div>
    </Provider>
  );
}

export default App;
