import store from "./store";
import { SubscriptionClient } from 'subscriptions-transport-ws'

const baseUrl = process.env.REACT_APP_API_URL;
const websocketUrl = process.env.REACT_APP_WS_URL;

let wsclient = null;

/**
 * Login using username and password.
 * @param {String} username Usermail
 * @param {String} password Password
 */
function login(username, password) {
    return new Promise((resolve, reject) => {
        const data = {
            query: `
            mutation {
                loginAppUser(username: "${username}", password: "${password}") {
                    token
                    user {
                        id
                        email
                        firstName
                        lastName
                        displayName
                        picture
                    }
                }
            }
            `
        };

        return fetch(baseUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result.data.loginAppUser) {
                    resolve(result.data.loginAppUser);
                    localStorage.setItem("loggedin", JSON.stringify(result.data.loginAppUser));
                } else {
                    reject(new Error(result.errors[0].message));
                }
            })
            .catch(reject);
    })
}

/**
 * Get all chats of logged in user.
 * @param {String} next Next Token
 * @param {Number} limit Number of row per request.
 */
function getChats(next, limit = 100) {
    return new Promise((resolve, reject) => {
        const data = {
            query: `
            query {
                appChats(limit: ${limit}${next ? `, next: "${next}"` : ""}) {
                    results {
                        id
                        title
                        icon
                        active
                    }
                    next
                    hasNext
                }
            }
            `
        };

        return fetch(baseUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${store.getState().loggedInToken}`
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result.data.appChats) {
                    resolve(result.data.appChats);
                } else {
                    reject(new Error(result.errors[0].message));
                }
            })
            .catch(reject);
    })
}

function getMessages(chatId, next, limit = 20) {
    return new Promise((resolve, reject) => {
        const data = {
            query: `
            query {
                appChatMessages(chatId: "${chatId}", limit: ${limit}${next ? `, next: "${next}"` : ""}) {
                    results {
                        id
                        body
                        sender {
                            id
                            displayName
                        }
                        createdAt
                    }
                    next
                    hasNext
                }
            }
            `
        };

        return fetch(baseUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${store.getState().loggedInToken}`
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result.data.appChatMessages) {
                    resolve(result.data.appChatMessages);
                } else {
                    reject(new Error(result.errors[0].message));
                }
            })
            .catch(reject);
    })
}

function sendMessage(chatId, body) {
    return new Promise((resolve, reject) => {
        const data = {
            query: `
            mutation {
                sendAppChatMessage(chatId: "${chatId}", message: "${body.trim()}") {
                    id
                }
            }
            `
        };

        return fetch(baseUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${store.getState().loggedInToken}`
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result.data.sendAppChatMessage) {
                    resolve(result.data.sendAppChatMessage);
                } else {
                    reject(new Error(result.errors[0].message));
                }
            })
            .catch(reject);
    })
}

function searchAppUser(search, next, limit) {
    return new Promise((resolve, reject) => {
        const data = {
            query: `
            query {
                searchAppUser(serach: "${search}") {
                    results {
                        id
                        firstName
                        lastName
                        displayName
                    }
                    next
                    hasNext
                    previous
                    nextPrevious
                }
            }
            `
        };

        return fetch(baseUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${store.getState().loggedInToken}`
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result.data.sendAppChatMessage) {
                    resolve(result.data.sendAppChatMessage);
                } else {
                    reject(new Error(result.errors[0].message));
                }
            })
            .catch(reject);
    })
}

function subscribeMessageSent(chatId, nextFn, errorFn) {
    wsclient = new SubscriptionClient(websocketUrl,
        {
            reconnect: true,
            connectionParams: {
                authToken: store.getState().loggedInToken
            }
        });

    const requestOption = {
        query: `
        subscription {
            appChatMessageSent(chatId: "${chatId}") {
                id
                body
                chatId
                sender {
                    id
                    displayName
                }
                createdAt
            }
        }
        `
    };

    return wsclient.request(requestOption).subscribe({
        next: payload => {
            if(nextFn && typeof nextFn === "function") {
                nextFn(payload.data.appChatMessageSent);
            }
        },
        error: errorFn
    });
}

function unsubscribe() {
    if(wsclient) {
        wsclient.unsubscribeAll();
        wsclient = null;
    }
}

export default {
    login,
    getChats,
    sendMessage,
    getMessages,
    searchAppUser,
    subscribeMessageSent,
    unsubscribe
} 