import { connect } from 'react-redux';
import { setSelectedChat } from '../store/actions';
import ChatList from '../components/ChatList';

const mapStateToProps = state => {
    return {
        selectedChat: state.selectedChat,
        chats: state.chats
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setSelectedChat: chat => {
            dispatch(setSelectedChat(chat));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatList);