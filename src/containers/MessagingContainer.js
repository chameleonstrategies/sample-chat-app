import { connect } from 'react-redux';
import { setSelectedChat } from '../store/actions';
import Messaging from '../components/Messaging';

const mapStateToProps = state => {
    return {
        selectedChat: state.selectedChat
    }
}

const mapDispatchToProps = dispatch => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Messaging);