import { connect } from 'react-redux';
import { logout } from '../store/actions';
import LogoutButton from '../components/LogoutButton';

const mapStateToProps = state => {
    return {
        loggedInUser: state.loggedInUser
    }
}

const mapDispatchToProps = dispatch => {
    return {
        logout: () => {
            dispatch(logout());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoutButton);