import { connect } from 'react-redux';
import { setLogin } from '../store/actions';
import LoginForm from '../components/LoginForm';

const mapDispatchToProps = dispatch => {
    return {
        setLogin: (user, token) => {
            dispatch(setLogin(user, token));
        }
    }
}

export default connect(null, mapDispatchToProps)(LoginForm);