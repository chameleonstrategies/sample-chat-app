import { connect } from 'react-redux';
import Chat from '../components/Chat';
import { setChats } from '../store/actions';

const mapStateToProps = state => {
    return {
        loggedInUser: state.loggedInUser,
        selectedChat: state.selectedChat
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setChats: chats => {
            dispatch(setChats(chats));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);