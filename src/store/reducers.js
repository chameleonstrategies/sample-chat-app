import { LOGIN, LOGOUT, SET_SELECTED_CHAT, SET_CHATS } from "./actionTypes";

const defaultState = {
    loggedInUser: null,
    loggedInToken: null,
    chats: [],
    selectedChat: null
};

const reduces = (state = defaultState, action) => {
    switch (action.type) {
        case LOGIN:
            return { ...state, loggedInUser: action.user, loggedInToken: action.token }
        case LOGOUT:
            return defaultState;
        case SET_SELECTED_CHAT:
            return { ...state, selectedChat: action.chat }
        case SET_CHATS:
            return { ...state, chats: action.chats }
        default:
            return state;
    }
}

export default reduces;