import { createStore } from 'redux';
import reducers from './reducers';
import { setLogin } from './actions';

const store = createStore(reducers)

// Preset logged in credential.
const loggedIn = JSON.parse(localStorage.getItem("loggedin"))
if(loggedIn) {
    store.dispatch(setLogin(loggedIn.user, loggedIn.token));
}

export default store;