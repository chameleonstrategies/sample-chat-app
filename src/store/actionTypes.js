export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const SET_SELECTED_CHAT = 'SET_SELECTED_CHAT';
export const SET_CHATS = 'SET_CHATS';