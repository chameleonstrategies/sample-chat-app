import { LOGIN, LOGOUT, SET_SELECTED_CHAT, SET_CHATS } from "./actionTypes"

export const setLogin = (user, token) => {
    return {
        type: LOGIN,
        user: user,
        token: token
    }
}

export const logout = () => {
    return {
        type: LOGOUT
    }
}

export const setChats = chats => {
    return {
        type: SET_CHATS,
        chats
    }
}
export const setSelectedChat = chat => {
    return {
        type: SET_SELECTED_CHAT,
        chat
    }
}